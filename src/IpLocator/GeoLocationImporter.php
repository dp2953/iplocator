<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
class GeoLocationImporter
{
    public $input_formats = array('json','csv','xml');
    
    /**
     * Build GeoLocation from a file input
     * 
     * @param string $inputFile
     * @param string $inputFormat
     * @return type
     * @throws \Exception
     * @throws \RuntimeException
     */
    public static function import($inputFile, $inputFormat)
    {
        if(!file_exists($inputFile) || !is_readable($inputFile)) {
            throw new \Exception("Enable to read the file, please check permissions");
        }
        
        //build GeoLocation object from data in the input file 
        $data = array();
        switch($inputFormat) {
            case 'json':
                $importer = new GeoLocationImporter\JsonImporter($inputFile);
                break;
            case 'xml':
                $importer = new GeoLocationImporter\XmlImporter($inputFile);
                break;
            case 'csv':
                $importer = new GeoLocationImporter\CsvImporter($inputFile);
                break;
            default:
                throw new \Exception("Invalid format");
        }

        return $importer->generateGeoLocation();
    }
    
}
