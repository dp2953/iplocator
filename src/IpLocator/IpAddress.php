<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
class IpAddress
{
    /** @type string */
    private $ipAddress;
    
    /**
     * Constructor for IpAddress object
     * 
     * @param string $addr represents an ip address
     * @throws \InvalidArgumentException Only IPV4 are currently supported
     */
    public function __construct($addr)
    {
        if (!filter_var($addr, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            throw new \InvalidArgumentException("Invalid IP Supplied, this function supprts only IPV4");
        }

        $this->ipAddress = $addr;
    }

    /**
     * Getter for the IpAddress
     * 
     * @return string Returns an ip string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Getter for the IpNumber
     * 
     * @return int Returns number version of IpAddress
     */
    public function getIpNumber()
    {
        $octets = explode(".", $this->ipAddress);
        return ($octets[0] << 24) + ($octets[1] << 16) + ($octets[2] << 8) + $octets[3];
    }

}
