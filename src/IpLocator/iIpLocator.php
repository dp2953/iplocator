<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
interface iIpLocator
{
    /**
     * Geo-locate the given IPV4 address.
     *
     * @param string $ip The IPV4 address
     * @return GeoLocation
     */
    public function geoLocate($ip);
}