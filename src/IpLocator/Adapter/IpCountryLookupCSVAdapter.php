<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator\Adapter;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
class IpCountryLookupCSVAdapter implements iIpCountryLookupAdapter
{

    /** @type array Holds data out of the CSV File*/
    private $_data = array();

    /**
     * Constructor parses the file and creates an array that can be searched later on
     * 
     * @param string $filename
     * @param char $delimiter
     * @return boolean
     */
    public function __construct($filename='', $delimiter=',')
    {
        //check if file excists and is readable
        if(!file_exists($filename) || !is_readable($filename)) {
            throw new \Exception("Enable to read the file, please check permissions");
        }
        
        //define the headers
        $header = array(
            "IP_FROM",
            "IP_TO",
            "COUNTRY_CODE",
            "COUNTRY_NAME"
        );
        
        //Loop through the file and create an array using IP_FROM as the key
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                $data_row = array_combine($header, $row);
                $data[$data_row['IP_FROM']] = $data_row;
            }
            fclose($handle);
        }
        $this->_data=$data;
    }

    /**
     * Do a quick search to see if ip number is in the supplied data
     * I tried to implement a faster way of doing it than just walking through array
     * 
     * @param array $array data we are searching
     * @param int $value value we are searching for
     * @return int
     */
    private function findClosestEntry($array,$value) {
        
        //if only one value found then it must match
        if(count($array) == 1 && current($array) == $value) {
            return $value;
        } elseif(count($array) <= 1) {
            //if array has one or less and it didnt match the value then return false
            return false;
        }
        
        //split array into 2
        list($head,$tail) = \array_chunk($array,count($array)/2);

        if (end($head) == $value) {
            //if last element in the head matched then we found exact entry
            return end($head);
        } elseif (current($tail)==$value) {
            //if first element in the tail of array matched then we found exact match
            return current($tail);
        } elseif(end($head)>$value) {
            //if last element in the head array is more than the value it means we need to search head array
            return $this->findClosestEntry($head, $value);
        } elseif (current($tail)<$value) {
            //if first element of the tail array is less than the value it means we need to search the tail array
            return $this->findClosestEntry($tail, $value);
        } elseif(end($head)<$value && current($tail)>$value) {
            //if value is between last element in head and first in the tail arrays then we use the last element in the head array
            return end($head);
        }
    }
    
    /**
     * Function returns a Country object
     * 
     * @param int $ip
     * @return \IpLocator\Country
     * @throws \OutOfBoundsException
     */
    public function findCountryByIpNumber($ip)
    {
        if(!is_numeric($ip)) {
            throw new \InvalidArgumentException("Invalid ip number supplied");
        }
        
        $keys = array_keys($this->_data);
        sort($keys);
        $lower_bound = $this->findClosestEntry($keys, $ip);
        
        if(!$lower_bound || !($this->_data[$lower_bound]['IP_TO']>=$ip)) {
            throw new \OutOfBoundsException("IP not found!");
        }
        
        //print_r($this->_data);
        return new \IpLocator\Country(
                $this->_data[$lower_bound]['COUNTRY_NAME'],
                $this->_data[$lower_bound]['COUNTRY_CODE']
        );
    }
}
