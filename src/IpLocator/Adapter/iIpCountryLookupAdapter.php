<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator\Adapter;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
interface iIpCountryLookupAdapter
{
    public function findCountryByIpNumber($ip);
}