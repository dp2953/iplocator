<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
class IpLocator implements iIpLocator
{
    
    private $_ipCountryLookup;

    public function __construct(Adapter\iIpCountryLookupAdapter $ipCountryLookup)
    {
        $this->_ipCountryLookup = $ipCountryLookup;
    }
    
    /**
     * Geo-locate the given IPV4 address.
     *
     * @param string $ip The IPV4 address
     * @return GeoLocation
     */
    public function geoLocate($ip) 
    {
        if (!($ip instanceof IpAddress)) {
            $ip = new IpAddress($ip);
        }
        
        return new GeoLocation($ip, $this->_ipCountryLookup->findCountryByIpNumber($ip->getIpNumber()));
    }
}