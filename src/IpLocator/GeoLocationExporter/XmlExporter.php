<?php

namespace IpLocator\GeoLocationExporter;

class XmlExporter extends ExporterAbstract
{

    /**
     * Converts array into xml
     * 
     * @param array $arr
     * @param \SimpleXMLElement $xml
     * @return \SimpleXMLElement
     */
    private function array_to_xml(array $arr, \SimpleXMLElement $xml)
    {
        foreach ($arr as $k => $v) {
            is_array($v)
                ? $this->array_to_xml($v, $xml->addChild($k))
                : $xml->addChild($k, $v);
        }
        return $xml;
    }
    
    public function export($objectArray) {
        return $this->array_to_xml(
                    $objectArray, 
                    new \SimpleXMLElement('<geo-location/>')
                )->asXML();
    }
}