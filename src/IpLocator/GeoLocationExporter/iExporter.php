<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator\GeoLocationExporter;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
interface iExporter
{

    /**
     * Array to export
     * 
     * @param array $array
     */
    public function export($array);
}