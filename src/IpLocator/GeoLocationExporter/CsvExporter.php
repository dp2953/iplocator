<?php

namespace IpLocator\GeoLocationExporter;

class CsvExporter extends ExporterAbstract
{
    /**
     * Converts array in csv
     * 
     * @param array $array
     * @return string
     */
    private function array_to_csv($array) {
        $string = "";
        foreach ($array as $key => $item) {
            if (in_array($key,array("ip","country"))) {
                foreach($item as $key_2=>$item_2) {
                    $string .= "\"{$key}_{$key_2}\",\"{$item_2}\"".PHP_EOL;
                }
            } else {
                $string .= "\"{$key}\",\"{$item}\"".PHP_EOL;
            }
        }

        return $string;
    }
    
    public function export($objectArray) {
        return $this->array_to_csv($objectArray);
    }
}