<?php

namespace IpLocator\GeoLocationExporter;

class JsonExporter extends ExporterAbstract
{
    public function export($objectArray) {
        return json_encode($objectArray);
    }
}