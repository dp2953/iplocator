<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
class GeoLocation
{
    /** @type IpAddress */
    private $ip;
    
    /** @type Country */
    private $country;
    
    //Variables that can be supported by ip2location library but not yet used
    /** @type string */
    private $regionName;
    
    /** @type string */
    private $cityName;
    
    /** @type string */
    private $latitude;
    
    /** @type string */
    private $longitude;
    
    /** @type string */
    private $isp;
    
    /** @type string */
    private $domainName;
    
    /** @type string */
    private $zipCode;
    
    /** @type string */
    private $timeZone;
    
    /** @type string */
    private $netSpeed;
    
    /** @type string */
    private $iddCode;
    
    /** @type string */
    private $areaCode;
    
    /** @type string */
    private $weatherStationCode;
    
    /** @type string */
    private $weatherStationName;
    
    /** @type string */
    private $mcc;
    
    /** @type string */
    private $mnc;
    
    /** @type string */
    private $mobileCarrierName;
    
    /** @type string */
    private $elevation;
    
    /** @type string */
    private $usageType;
    
    /** @type string Represents default output when printing out the object */
    private $exporter;

    /**
     * Create GeoLocation, minim requires IP and Country
     * 
     * @param \IpLocator\IpAddress $ip
     * @param \IpLocator\Country $country
     */
    public function __construct(IpAddress $ip, Country $country){
        $this->ip = $ip;
        $this->country = $country;
    }
    
    /**
     * General setter function
     * 
     * @param string $property
     * @param string $value
     * @return \IpLocator\GeoLocation
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    /**
     * @return \IpLocator\IpAddress 
    */
    public function getIpAddress()
    {
        return $this->ip;
    }

    /**
     * @return \IpLocator\Country
    */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set the exporter you would like the geo location to be exported out as
     * 
     * @param iExporter $exporter
     */
    public function setExporter(GeoLocationExporter\iExporter $exporter) {
        $this->exporter = $exporter;
        return $this;
    }
    
    /**
     * Overloaded funtion to print out the Geo Location in the format supplied to the class
     * @return string
     */
    public function __toString() {
        if(isset($this->exporter) && ($this->exporter instanceOf GeoLocationExporter\iExporter)) {
            return $this->exporter->export($this->objectToArray());
        } else {
            return var_export($this->objectToArray(),true);
        }
    }
    
    /**
     * Convert Geo location into the array that we can use later on
     * 
     * @return array
     */
    private function objectToArray() {
        return array(
            'ip' => array('ipAddress'=>$this->ip->getIpAddress()),
            'country' => array('code'=>$this->country->getCode(), 'name'=>$this->country->getName()),
            'regionName' => $this->regionName,
            'cityName' => $this->cityName,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'isp' => $this->isp,
            'domainName' => $this->domainName,
            'zipCode' => $this->zipCode,
            'timeZone' => $this->timeZone,
            'netSpeed' => $this->netSpeed,
            'iddCode' => $this->iddCode,
            'areaCode' => $this->areaCode,
            'weatherStationCode' => $this->weatherStationCode,
            'weatherStationName' => $this->weatherStationName,
            'mcc' => $this->mcc,
            'mnc' => $this->mnc,
            'mobileCarrierName' => $this->mobileCarrierName,
            'elevation' => $this->elevation,
            'usageType' => $this->usageType
        );
    }
    
    
}
