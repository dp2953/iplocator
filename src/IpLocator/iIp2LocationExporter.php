<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
interface iIp2LocationExporter
{
    /**
     * @param string $inputFile     Path to input file
     * @param string $inputFormat   Format of the input file (json, csv, xml, etc…)
     * @param string $outputFile    Path to output file
     * @param string $outputFormat  Format of the output file (json, csv, xml, etc…)
     */
    public function export($inputFile, $inputFormat, $outputFile,  $outputFormat);
}
