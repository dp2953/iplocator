<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
class Country
{
    /** @type string */
    private $name;
    
    /** @type string char(2) */
    private $code;

    /**
     * Constructor to set up Country object
     * 
     * @param string $name
     * @param string $code
     * @throws \InvalidArgumentException
     */
    public function __construct($name, $code)
    {
        if (!is_string($name) || empty($name)) {
            throw new \InvalidArgumentException("Country name supplied must be a string");
        }

        if (!is_string($code) || strlen($code) != 2) {
            throw new \InvalidArgumentException("Country code supplied must be 2 characters long string");
        }
        
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * Return a name of the Country
     * 
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return a Code of the country
     * 
     * @return string char(2)
     */
    public function getCode()
    {
        return $this->code;
    }   
}