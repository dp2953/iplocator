<?php

namespace IpLocator\GeoLocationImporter;

class CsvImporter extends ImporterAbstract
{
    public function __construct($inputFile) {
        $this->isFileReadable($inputFile);
        
        $file = file_get_contents($inputFile);
        $output = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $file));
        foreach($output as $vals) {
            $data[$vals[0]] = $vals[1];
        }

        $this->data = $data;
    }
}