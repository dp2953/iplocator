<?php

namespace IpLocator;
namespace IpLocator\GeoLocationImporter;

abstract class ImporterAbstract
{
    protected $data = array();
    
    public function isFileReadable($filename)
    {
        if(!file_exists($filename) || !is_readable($filename)) {
            throw new \Exception("Failed to read the file, please check permissions");
        }
        
        return true;
    }
    
    /**
     * Build GeoLocation from an array of values
     * 
     * @param array $data
     * @return \IpLocator\GeoLocation
     * @throw \InvalidArgumentException
     * @throws \Exception
     */
    public function generateGeoLocation()
    {
        $data = $this->data;
        
        if (!is_array($data)) {
            throw new \InvalidArgumentException("Invalid argument supplied");
        }
        
        $ipAddress = null;
        if(array_key_exists('ip', $data) && array_key_exists('ipAddress', $data['ip'])) {
            $ipAddress = $data['ip']['ipAddress'];
            unset($data['ip']);
        } elseif (array_key_exists('ip_ipAddress', $data)) {
            $ipAddress = $data['ip_ipAddress']; 
            unset($data['ipAddress']);
        }
        
        if (!$ipAddress) {
            throw new \Exception("Ip not founds");
        }

        $ip = new \IpLocator\IpAddress($ipAddress);
        
        $country = null;
        if(
            array_key_exists('country', $data)
            && array_key_exists('name', $data['country'])
            && array_key_exists('code', $data['country']) 
        ) {
            $country = new \IpLocator\Country($data['country']['name'], $data['country']['code']);
            unset($data['country']);
        } elseif(array_key_exists('country_code', $data) && array_key_exists('country_name', $data)) {
            $country = new \IpLocator\Country($data['country_name'], $data['country_code']);
            unset($data['country_name']);
            unset($data['country_code']);
        }

        $geo_location = new \IpLocator\GeoLocation($ip, $country);
        
        foreach ($data as $key => $param) {
            $geo_location->{$key} = $param;
            
        }
        
        unset($data);
        return $geo_location;
    }
}