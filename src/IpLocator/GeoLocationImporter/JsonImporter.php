<?php

namespace IpLocator\GeoLocationImporter;

class JsonImporter extends ImporterAbstract
{
    public function __construct($inputFile) {
        $this->isFileReadable($inputFile);
        
        $string = file_get_contents($inputFile);
        if(!self::isJson($string)){
            throw new \Exception("Invalid json supplied");
        }
        $this->data = json_decode($string,true);
    }
    
    
    /**
     * Check if output is Json
     * 
     * @param type $string
     * @return type
     */
    private static function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}