<?php

namespace IpLocator\GeoLocationImporter;

class XmlImporter extends ImporterAbstract
{
    public function __construct($inputFile) {
        $this->isFileReadable($inputFile);
        
        $xml = simplexml_load_file($inputFile);
        $json = json_encode($xml);
        $this->data = json_decode($json,TRUE);
    }
}