<?php

/**
 * This file is part of the IpLocator package
 */

namespace IpLocator;

/**
 * @author Dimitrij Phoursa <d.phoursa@outlook.com>
 */
class Ip2LocationExporter implements iIp2LocationExporter
{
    public $input_formats = array('json','csv','xml');
    
    /**
     * @param string $inputFile     Path to input file
     * @param string $inputFormat   Format of the input file (json, csv, xml, etc…)
     * @param string $outputFile    Path to output file
     * @param string $outputFormat  Format of the output file (json, csv, xml, etc…)
     */
    public function export($inputFile, $inputFormat, $outputFile,  $outputFormat)
    {
        $geo_location = GeoLocationImporter::import($inputFile, $inputFormat);

        switch ($outputFormat) {
            case 'xml':
                $exporter = new GeoLocationExporter\XmlExporter;
                break;
            case 'json':
                $exporter = new GeoLocationExporter\JsonExporter;
                break;
            case 'csv':
                $exporter = new GeoLocationExporter\CsvExporter;
                break;
            default:
                throw new \InvalidArgumentException("Unsupported export format requested");
        }
        
        $geo_location->setExporter($exporter);
        
        //Check we can write into the file
        if(!($file = fopen($outputFile,"w"))) {
            throw new \InvalidArgumentException("Output file can't be written");
        }
        //write the string into the file
        fwrite($file,$geo_location);
        
        //release the resource
        fclose($file);
        
        return true;
    }
}
