<?php

namespace IpLocator\Tests;

require_once __DIR__.'/../../../src/autoload.php';

use IpLocator;
use IpLocator\Adapter;

class IpLocatorTest extends \PHPUnit_Framework_TestCase
{
    /** @type \IpLocator\IpLocator */
    private $ip_locator;
    
    protected function setUp()
    {
        $csv_finder = new \IpLocator\Adapter\IpCountryLookupCSVAdapter("ipcountry.sample.csv");
        $this->ip_locator = new \IpLocator\IpLocator($csv_finder); 
    }
    public function testInstanceOf()
    {
        $this->assertInstanceOf('\IpLocator\IpLocator',$this->ip_locator);
    }
    
    
    public function testGeoLocateInvalidParamException()
    {
        $this->setExpectedException(
                'InvalidArgumentException', 'Invalid IP Supplied, this function supprts only IPV4'
        );
        
        $this->ip_locator->geoLocate("72.77.138.60.22");
    }
    
    public function testGeoLocateNotFound()
    {
        $this->setExpectedException(
            'OutOfBoundsException', 'IP not found!'
        );
                
        $this->ip_locator->geoLocate("72.77.138.60");
    }
    
    public function testGeoLocateOutputInstance()
    {
        $geo_location = $this->ip_locator->geoLocate("5.32.160.0");
        
        $this->assertTrue(($geo_location instanceof \IpLocator\GeoLocation));
    }
    
    public function testGeoLocateOutput()
    {
        //expecting to have code="DE" and name="GERMANY" in the country object
        $geo_location = $this->ip_locator->geoLocate("5.32.160.0");
        $country = $geo_location->getCountry();
        
        $this->assertTrue($country->getName() == "GERMANY" && $country->getCode() == "DE");
    }
}