<?php

namespace IpLocator\Tests;

require_once __DIR__.'/../../../src/autoload.php';

use IpLocator;
use IpLocator\Adapter;

class GeoLocationTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp(){}

    public function testGeoLocationCreation()
    {
        $geo_location = new \IpLocator\GeoLocation(
                new \IpLocator\IpAddress("5.32.160.0"),
                new \IpLocator\Country("GERMANY","DE")
        );
        
        $this->assertInstanceOf("\IpLocator\GeoLocation",$geo_location);
    }
    
    
}