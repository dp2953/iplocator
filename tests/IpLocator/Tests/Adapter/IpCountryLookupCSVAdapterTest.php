<?php

namespace IpLocator\Tests;

require_once __DIR__.'/../../../../src/autoload.php';

use IpLocator;
use IpLocator\Adapter;

class IpCountryLookupCSVAdapterTest extends \PHPUnit_Framework_TestCase
{
    /** @type \IpLocator\IpLocator */
    private $csv_finder;
    
    protected function setUp()
    {
        $this->csv_finder = new \IpLocator\Adapter\IpCountryLookupCSVAdapter('ipcountry.sample.csv');
    }
    
    public function testFindCountryByIpNumberInvalid()
    {
        $this->setExpectedException(
                'InvalidArgumentException', 'Invalid ip number supplied'
        );
        
        $this->csv_finder->findCountryByIpNumber("string");
    }
}