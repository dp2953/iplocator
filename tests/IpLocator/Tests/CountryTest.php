<?php

namespace IpLocator\Tests;

require_once __DIR__.'/../../../src/autoload.php';

use IpLocator;
use IpLocator\Adapter;

class CountryTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp(){}

    public function testCountryInvalidName()
    {
        $this->setExpectedException(
                'InvalidArgumentException', 'Country name supplied must be a string'
        );
        
        new \IpLocator\Country(12,"DE");
    }
    
    public function testCountryInvalidCode()
    {
        $this->setExpectedException(
                'InvalidArgumentException', 'Country code supplied must be 2 characters long string'
        );
        
        new \IpLocator\Country("GERMANY","D");
    }
}