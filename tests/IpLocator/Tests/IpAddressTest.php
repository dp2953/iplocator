<?php

namespace IpLocator\Tests;

require_once __DIR__.'/../../../src/autoload.php';

use IpLocator;
use IpLocator\Adapter;

class IpAddressTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp(){}

    public function testIpInvalidParamException()
    {
        $this->setExpectedException(
                'InvalidArgumentException', 'Invalid IP Supplied, this function supprts only IPV4'
        );
        
        new \IpLocator\IpAddress("72.77.138.60.22");
    }
    
    public function testIpOutput()
    {
        //expecting to have code="DE" and name="GERMANY" in the country object
        $ipAddress = new \IpLocator\IpAddress("5.32.160.0");
        
        $this->assertEquals($ipAddress->getIpNumber(),86024192);
    }
}