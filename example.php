<?php

require_once 'src/autoload.php';

$csv_finder = new IpLocator\Adapter\IpCountryLookupCSVAdapter("ipcountry.sample.csv");
$ip_locator = new IpLocator\IpLocator($csv_finder);

$geolocation = $ip_locator->geoLocate("5.32.160.0");

print($geolocation);

$ip = new IpLocator\Ip2LocationExporter;
$ip->export("geo-location.input.csv","csv","geo-location.output.xml","xml");
