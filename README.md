IpLocator
========

**IpLocator** has been create for Social Point interview process, stage 2

Things still to do: I need to have better test coverage

Installation
------------

Currently the code is supplied in the zip file, however it is hosted on BitBucket and with addition of comploser.json file can be cloned from the repository


Usage
-----

Example usage of the library:
```php
<?php

require_once 'src/autoload.php';

//Question 1

//Set up the Adapter that will use supplied csv file for searching ip addresses
$csv_finder = new \IpLocator\Adapter\IpCountryLookupCSVAdapter("ipcountry.sample.csv");

//initialise IpLoader library
$ip_locator = new \IpLocator\IpLocator($csv_finder);

//get geo location of the ip
$geolocation = $ip_locator->geoLocate("5.32.160.0");

//by default printing the class will output var_export of the array object
print($geolocation);

//Question 2

//initialise Exporter
$ip = new \IpLocator\Ip2LocationExporter;

//run the export on the geo location
//currently you can only have one geo location in the file
$ip->export("geo-location.input.json","json","geo-location.output.xml","xml");
```

Running Test:
(More tests will be added)
```
phpunit tests
```

Possible formats of the input files:

JSON:
```
{
    "ip": {
        "ipAddress": "5.32.160.0"
    },
    "country": {
        "code": "UK",
        "name": "UNITED KINGDOM"
    }
}
```

CSV:
```
"ip_ipAddress","5.32.160.0"
"country_code","UK"
"country_name","UNITED KINGDOM"
"regionName",""
"cityName",""
"latitude",""
"longitude",""
"isp",""
"domainName",""
"zipCode",""
"timeZone",""
"netSpeed",""
"iddCode",""
"areaCode",""
"weatherStationCode",""
"weatherStationName",""
"mcc",""
"mnc",""
"mobileCarrierName",""
"elevation",""
"usageType",""
```

XML:
```
<?xml version="1.0" encoding="utf-8"?>
<geo-location>
    <ip>
        <ipAddress>5.32.160.0</ipAddress>
    </ip>
    <country>
        <code>DE</code>
        <name>GERMANY</name>
    </country>
</geo-location>
```